// Link to test answer on LeetCode: https://leetcode.com/problems/intersection-of-two-arrays-ii/?envType=featured-list

// Intersection of Two Arrays Problem: We are given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must appear as many times as it shows in both arrays. Provide two different solutions for us to choose from.